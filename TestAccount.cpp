/*
File	: TestAccount.cpp
Poject	: cpp_test_bank_account
===============================================================
<< History Revision >>
Date			Author		Contents
2020-04-30		Pipit L.	First released
===============================================================
*/

#include<iostream>
#include <string>
#include "account.h"

using namespace std;

int main(void)
{
	/* Create first bank account and initial balance */
	Bank *first = new Bank("Pipit", 25325);
	double tempDeposit = 5325;
	double tempWithdraw = 30000;

	cout << "********************************" << endl;
	cout << "*         FIRST ACCOUNT        *" << endl;
	cout << "********************************" << endl;
	cout << "Create Account  : " << first->accessName(first) << endl;				/* Pipit */
	cout << "Check Balance   : " << first->checkBalance(first) << endl;				/* 25325 */
	cout << "+++ Deposit +++ : " << tempDeposit << endl;							/* +5325 */
	cout << "Check Balance   : " << first->deposit(first, tempDeposit) << endl;		/* 30650 */
	cout << "--- Withdraw ---: " << tempWithdraw << endl;							/* -30000 */
	cout << "Check Balance   : " << first->withdraw(first, tempWithdraw) << endl;	/* 650 */
	delete first;

	/* Create second bank account and initial balance */
	Bank *second = new Bank("Longloy", 0.6);
	tempDeposit = 987654321.4;
	tempWithdraw = 987654322;

	cout << "*********************************" << endl;
	cout << "*         SECOND ACCOUNT        *" << endl;
	cout << "*********************************" << endl;
	cout << "Create Account  : " << second->accessName(second) << endl;				/* Longloy */
	cout << "Check Balance   : " << second->checkBalance(second) << endl;			/* 0.6 */
	cout << "+++ Deposit +++ : " << tempDeposit << endl;							/* +987654321.4 */
	cout << "Check Balance   : " << second->deposit(second, tempDeposit) << endl;	/* 987654322 */
	cout << "--- Withdraw ---: " << tempWithdraw << endl;							/* -987654322 */
	cout << "Check Balance   : " << second->withdraw(second, tempWithdraw) << endl;	/* 0 */
	delete second;

	system("pause");
	return 0;
}