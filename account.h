/*
File	: account.h
Poject	: cpp_test_bank_account
===============================================================
<< History Revision >>
Date			Author		Contents
2020-04-30		Pipit L.	First released
===============================================================
*/

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <string>

using namespace std;

class Bank
{
private:
	string customerName;
	double balance;
public:
	Bank(string _name_, double _balance_);
	string accessName(Bank *pBANK);
	double checkBalance(Bank *pBANK);
	double deposit(Bank *pBANK, double _amount_);
	double withdraw(Bank *pBANK, double _amount_);
};

#endif // ACCOUNT_H


