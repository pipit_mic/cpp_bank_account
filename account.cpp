/*
File	: account.cpp
Poject	: cpp_test_bank_account
===============================================================
<< History Revision >>
Date			Author		Contents
2020-04-30		Pipit L.	First released
===============================================================
*/

#include <iostream>
#include <string>
#include "account.h"

using namespace std;

Bank::Bank(string _name_, double _balance_)
{
	customerName = _name_;
	balance = _balance_;
}

string Bank::accessName(Bank *pBANK)
{
	return pBANK->customerName;
}

double Bank::checkBalance(Bank *pBANK)
{
	return pBANK->balance;
}

double Bank::deposit(Bank *pBANK, double _amount_)
{
	pBANK->balance = pBANK->balance + _amount_;
	return pBANK->balance;
}

double Bank::withdraw(Bank *pBANK, double _amount_)
{
	pBANK->balance = pBANK->balance - _amount_;
	return pBANK->balance;
}